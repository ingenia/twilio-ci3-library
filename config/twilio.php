<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Account SID
 */
$config['twilio_account_sid'] = '';
/*
 * Auth Token
 */
$config['twilio_auth_token']	= '';
/*
 * Twilio Phone Number
 */
$config['twilio_default_number']	= '';