# README #

Libreria de Twilio para Codeigniter 3

### INDICE ###

* Que hace
* Agradecimientos

### QUE HACE? ###

V.01
-------
 - Puedes hacer tu instancia para obtener toda la funcionalidad de la 
   libreria como lo menciona Twilio: `get_twilio_service()`
 - opción de enviar sms : `send_sms($msj, $to, $from)`

### GRACIAS A ###

Esta libreria está basada en la libreria de Ben Edmunds:
https://github.com/benedmunds/CodeIgniter-Twilio

En la idea de helper de Shawn Parker
http://blog.openvbx.org/2011/08/using-the-new-twilio-services-library-in-a-codeigniter-application/

y "The Twilio PHP Helper Library"
https://www.twilio.com/docs/php/install 

### SALUDOS ###
[ Ingenia Dev Team ](http://ingeniaoaxaca.com)