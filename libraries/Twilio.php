<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/Service_twilio/Twilio.php';

class Twilio
{
	protected $_ci;
	protected $account_sid;
	protected $auth_token;
	protected $default_number;
	protected $twilio_service;

  function __construct()
  {
  	//initialize the CI super-object
		$this->_ci =& get_instance();
		
		//load config
		$this->_ci->load->config('twilio', TRUE);
		
		//get settings from config
		$this->account_sid 		= $this->_ci->config->item('twilio_account_sid', 'twilio');
		$this->auth_token  		= $this->_ci->config->item('twilio_auth_token', 'twilio');
		$this->default_number = $this->_ci->config->item('twilio_default_number', 'twilio');

		$this->twilio_service = $this->get_twilio_service();
  }

  function get_twilio_service() 
  {
		static $twilio_service;

		if (!($twilio_service instanceof Services_Twilio)) 
			$twilio_service = new Services_Twilio($this->account_sid, $this->auth_token);

		return $twilio_service;
	}
	/*
	Use example:

	$this->twilio->send_sms('hello :D', '+521<your_number>');
	$this->twilio->send_sms('bye :(', '+521<your_number>', '<another_number>');
	 */
	function send_sms($msj='', $to, $from=FALSE){
		$result = FALSE;

		if(!$from)
			$from = $this->default_number;

		try {
			$result = $this->twilio_service->account->messages->create(array( 
				'To' => $to, 
				'From' => $from, 
				'Body' => $msj
			));
		} catch (Exception $e) {
			$result = $e;
		}

		return $result;
	}

	//ADD ANOTHER GENERAL FUNCTIONS
}